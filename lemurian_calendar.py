# lemurian_calendar.py - Lemurian calendar conversions.
# Copyright 2018 Rob Myers <rob@robmyers.org>
# Licensed under The MIT License (MIT)
# http://opensource.org/licenses/MIT

################################################################################
#
# USEFUL KNOWN DATES
# https://twitter.com/dblbd/status/743630339222839296
# 17 Jun 2016
# 122112
# date(2016, 6, 17) - timedelta(ternary_to_decimal(122112))
# = datetime.date(2016, 2, 29)
#
# https://twitter.com/dblbd/status/746699732525842437
# 25 Jun 2016
# 122211
#
################################################################################

#TODO: Type the dates rather than string prefixing feast days?
#      (number, day|holiday, cycle[where 0 starts in 2016])

from __future__ import print_function

from datetime import date, timedelta

MU_DOUBLE_YEAR_PERIOD = (3 ** 6)
MU_INTERCALATION_DAYS = 3
MU_INTERCALATION_EVERY = 2
MU_TWO_CYCLES = MU_DOUBLE_YEAR_PERIOD * MU_INTERCALATION_EVERY
MU_TWO_CYCLES_PLUS_INTERCALATION = MU_TWO_CYCLES + MU_INTERCALATION_DAYS
MU_AEON_CYCLES = 27
MU_AEON = (MU_TWO_CYCLES_PLUS_INTERCALATION * 27) // 2

MU_INTERCALATION_DAY_PREFIX = 'Intercalation day '

# We know this is the start of a cycle.
#FIXME: Was there an intercalation before this? We assume yes
#       If not, change this so there was.
MU_EPOCH = date(2016, 2, 29)

def mu_ternary_to_decimal(ternary):
    """Base 3 integer to base 10, zero-indexed integer (0..728 not 1..729)"""
    s = str(ternary)
    decimal = 0
    num_digits = len(s)
    for i in range(0, num_digits):
        index = num_digits - i - 1
        decimal += (int(s[index]) - 1) * pow(3, i)
    return decimal

# From @cyborg_nomade's code
def _mu_decimal_to_ternary_string(n):
    """Decimal integer to ternary string"""
    e = n//3
    q = n%3
    if n == 0:
        return '0'
    elif e == 0:
        return str(q)
    else:
        return _mu_decimal_to_ternary_string(e) + str(q)

# From @cyborg_nomade's code
def mu_decimal_to_ternary(n, width=6):
    """Convert decimal integer n to ternary integer"""
    return int(_mu_decimal_to_ternary_string(n)) + int('1' * width)

def mu_ternary_add_decimal(ternary, amount):
    """Add decimal amount to ternary (negative subtracts)"""
    i = mu_ternary_to_decimal(ternary)
    return mu_decimal_to_ternary(i + amount)

# def mu_gregorian_to_zygotriadic(gregorian):
#     """Convert the Gregorian date to the Lemurian"""
#     # Positive or negative difference from the last known cycle start
#     days = (gregorian - MU_EPOCH).days
#     # Day within two-cyles-plus-intercalation time period
#     day = days % MU_TWO_CYCLES_PLUS_INTERCALATION
#     # Make sure we account for intercalation when counting two-year cycles
#     cycles = (days // MU_TWO_CYCLES_PLUS_INTERCALATION) * 2
#     if day > MU_TWO_CYCLES:
#         day = MU_INTERCALATION_DAY_PREFIX \
#             + str((day % MU_TWO_CYCLES) + 1)
#         cycles = cycles + 1
#     elif day > MU_DOUBLE_YEAR_PERIOD:
#         day = mu_decimal_to_ternary(day % MU_DOUBLE_YEAR_PERIOD, 6)
#         cycles = cycles + 1
#     else:
#         day = mu_decimal_to_ternary(day, 6)
#     cycle = cycles % MU_AEON_CYCLES
#     return '%s:%s' % (mu_decimal_to_ternary(cycle, 3), day)

def _mu_gregorian_to_zygotriadic_before_epoch(gregorian):
    """Get the Lemurian date before the cycle starting in 2016"""
    # Just abs the days now rather than keeps absing them to compare against
    # quantities that are expressed as positive constants.
    days = abs((gregorian - MU_EPOCH).days) - 1
    #FIXME: Reduce intercalation by one every 64 twin-year cycles
    # Note that cycles go down, we convert them to a positive
    # amount using modulus.
    cycle = 0
    while days >= 0:
        cycle -= 1
        if (cycle % MU_INTERCALATION_EVERY) == 1:
            if days < MU_INTERCALATION_DAYS:
                result = MU_INTERCALATION_DAY_PREFIX + str(MU_INTERCALATION_DAYS - days)
                break
            days -= MU_INTERCALATION_DAYS
        if days < MU_DOUBLE_YEAR_PERIOD:
            result = mu_decimal_to_ternary((- days) % MU_DOUBLE_YEAR_PERIOD)
            break
        if days < MU_DOUBLE_YEAR_PERIOD + MU_INTERCALATION_DAYS:
            result = mu_decimal_to_ternary((- days) % MU_DOUBLE_YEAR_PERIOD)
            break
        days -= MU_DOUBLE_YEAR_PERIOD
    return '%s:%s' % (mu_decimal_to_ternary(cycle % MU_AEON_CYCLES, 3), result)

def _mu_gregorian_to_zygotriadic_after_epoch(gregorian):
    """Get the Lemurian date after the cycle starting in 2016"""
    days = (gregorian - MU_EPOCH).days
    #FIXME: Reduce intercalation by one every 64 twin-year cycles
    cycle = 0
    while days >= 0:
        if days < MU_DOUBLE_YEAR_PERIOD:
            result = mu_decimal_to_ternary(days)
            break
        days -= MU_DOUBLE_YEAR_PERIOD
        cycle += 1
        if (cycle % MU_INTERCALATION_EVERY) == 0:
            if days < MU_INTERCALATION_DAYS:
                result = MU_INTERCALATION_DAY_PREFIX + str(days + 1)
                # Holidays go after the previous cycle, not at the start of the next
                cycle = cycle - 1
                break
            days -= MU_INTERCALATION_DAYS
    return '%s:%s' % (mu_decimal_to_ternary((cycle % MU_AEON_CYCLES), 3),
                      result)

def mu_gregorian_to_zygotriadic(gregorian):
    """Convert the Gregorian date to the Lemurian"""
    if gregorian >= MU_EPOCH:
        result = _mu_gregorian_to_zygotriadic_after_epoch(gregorian)
    else:
        result = _mu_gregorian_to_zygotriadic_before_epoch(gregorian)
    return result

def mu_zygotriadic_to_gregorian(ternary, aeon='111'):
    """Get the nth occurrence of the ternary date,
       where 0 is relative to the cycle starting 2016-02-29
       NTH IS CURRENTLY IGNORED"""
    #TODO: implement past nth
    aeon_index = mu_ternary_to_decimal(aeon)
    aeon_days = (aeon_index // 2) * MU_TWO_CYCLES_PLUS_INTERCALATION
    if aeon_index % 2:
        aeon_days += MU_DOUBLE_YEAR_PERIOD
    return MU_EPOCH + timedelta(aeon_days + mu_ternary_to_decimal(ternary))

if __name__ == '__main__':
    import sys
    if len(sys.argv) == 1:
        print('Possible arguments:')
        print('  <nothing>  - Print this help message.')
        print('  2018-01-27 - Convert a Gregorian date to Lemurian.')
        print('  112233     - Convert a Lemurian date to Gregorian.')
        print('  123:112233 - Convert a Lemurian date to Gregorian.')
    elif '-' in sys.argv[1]:
        d = [int(element) for element in sys.argv[1].split('-')]
        gregorian = date(*d)
        print(mu_gregorian_to_zygotriadic(gregorian))
    elif ':' in sys.argv[1]:
        date_components = sys.argv[1].split(':')
        print(mu_zygotriadic_to_gregorian(date_components[0], date_components[1]))
    elif len(sys.argv[1]) == 6:
        print(mu_zygotriadic_to_gregorian(sys.argv[1]))
