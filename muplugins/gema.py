import re
import urllib

from rtmbot.core import Plugin

URL_BASE = 'https://www.urbanomic.com/glossget.php?status=idle&system=AQ&wait=0&glossary='

RECOGNIZED = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'
CLEAN = r'[^0-9A-Z+]'
LOOKUP = dict(zip(list(RECOGNIZED), range(0, len(RECOGNIZED))))

def clean(dirty):
    return re.sub('\++',
                  '+',
                  re.sub(CLEAN,
                         '',
                         re.sub(r'\s+',
                                '+',
                                dirty.upper().strip())))

def calculate(text):
    value = 0
    for a in list(text.upper()):
        value += LOOKUP.get(a, 0)
    return value

def digits(num):
    digits = []
    while num > 0:
        digits.append(num % 10)
        num = int(num / 10)
    digits.reverse()
    return digits

def decimalReduce(value):
    values = [value]
    while value > 10:
        value = sum(digits(value))
        values.append(value)
    return values

def query(cleanPhrase, results):
    q = '&phrase=%s' % cleanPhrase
    index = 1
    for result in results:
        q += '&result%i=%s' % (index, result)
        index += 1
    return q

def glossGet(cleanPhrase, reductions):
  url = URL_BASE + query(cleanPhrase, reductions)
  # Convert bytes-like object to string
  return urllib.urlopen(url).read().decode('utf-8')

def glossParse(body):
    values = dict()
    for line in body.splitlines():
        matches = re.match(r'^(\d+)=(.+)$', line)
        if matches:
            value = int(matches.group(1))
            phrase = matches.group(2)
            if not values.get(value, False):
                values[value] = []
            values[value].append(phrase)
    return values

def fetchAndDescribeEquivalences(phrase):
    cleanPhrase = clean(phrase)
    value = calculate(cleanPhrase)
    reductions = decimalReduce(value)
    body = glossGet(cleanPhrase, reductions)
    equivalences = glossParse(body)
    return "%s (%i) = %s" % (phrase, value, ' = '.join(equivalences[value]))

HELP = """
gema <phrase> - calculate the AQ of that phrase and query the gematrix for it.
"""

class AnglossicQabbala(Plugin):

    def process_message(self, data):
        message = False
        response = False
        my_id = self.slack_client.server.login_data['self']['id']
        at_message_prefix = u'<@{0}>'.format(my_id)
        # Respond to DMs or addresses
        if data['channel'].startswith('D'):
            message = data['text']
        elif data['text'].startswith(at_message_prefix):
            message = data['text'][len(at_message_prefix):].strip()
        if message:
            if message.lower() == 'help':
                response = HELP
            elif message.startswith('gema'):
                matches = re.match(r'^\s*gema\s+(.+)\s*$', message)
                if matches:
                    phrase = matches.group(1)
                    response = fetchAndDescribeEquivalences(phrase)
                else:
                    response = HELP
        if response:
            self.outputs.append([data['channel'], response])
