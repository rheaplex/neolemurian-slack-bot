# mucalendar.py - Lemurian calendar Slack bot plugin.
# Copyright 2018 Rob Myers <rob@robmyers.org>
# Licensed under The MIT License (MIT)
# http://opensource.org/licenses/MIT

from __future__ import unicode_literals

import datetime

from rtmbot.core import Plugin, Job

import lemurian_calendar


class MuCalendarDayAnnouncerJob(Job):

    def run(self, slack_client):
        today = datetime.date.today()
        lemurian =  str(lemurian_calendar.mu_gregorian_to_zygotriadic(
            today))
        response = "Today is " + lemurian
        return [[channel, response] for channel in self.announce_channels]


HELP = """help
today
2018-01-28
333333
333:333333"""

class MuCalendarPlugin(Plugin):

#    def register_jobs(self):
#        job = MuCalendarDayAnnouncerJob(60*60*24)
#        job.announce_channels = self.config['ANNOUNCE_DAY_CHANNELS']
#        self.jobs.append(job)

    def process_message(self, data):
        message = False
        response = False
        my_id = self.slack_client.server.login_data['self']['id']
        at_message_prefix = u'<@{0}>'.format(my_id)
        # Respond to DMs or addresses
        if data['channel'].startswith('D'):
            message = data['text']
        elif data['text'].startswith(at_message_prefix):
            message = data['text'][len(at_message_prefix):].strip()
        if message:
            if message.lower() == 'help':
                # Help
                response = HELP
            elif message.lower() == 'today':
                # Today
                today = datetime.date.today()
                lemurian =  str(lemurian_calendar.mu_gregorian_to_zygotriadic(
                    today))
                response = "Today is " + lemurian
            elif '-' in message:
                # 2018-01-28
                gregorian = datetime.date(*[int(element)
                                            for element in message.split('-')])
                lemurian = str(lemurian_calendar.mu_gregorian_to_zygotriadic(
                    gregorian))
                response = str(gregorian) + ' = ' + lemurian
            elif ':' in message:
                # 123:112233
                mu = message.split(':')
                gregorian = str(lemurian_calendar.mu_zygotriadic_to_gregorian(
                    mu[1], mu[0]))
                response = message + ' = ' + gregorian
            elif len(message) == 6:
                # 112233
                gregorian = str(lemurian_calendar.mu_zygotriadic_to_gregorian(
                    message))
                response = message + ' = ' + gregorian
        if response:
            self.outputs.append([data['channel'], response])
