# mucalendar.py - Lemurian calendar Slack bot plugin.
# Copyright 2018 Rob Myers <rob@robmyers.org>
# Licensed under The MIT License (MIT)
# http://opensource.org/licenses/MIT

from __future__ import unicode_literals

import datetime

from rtmbot.core import Plugin, Job

import lemurian_necronomicon as ln

################################################################################
# CLI
################################################################################

HELP = """mesh <number> - Show the details for the demon with that mesh number.
mesh <name> - Show the details for the demon with that name.
mesh name <number> - Show the demon's name.
mesh names <number> - Show the demon's names.
mesh clicks <number> - Show what the demon's clicks (usually nothing).
mesh pitch <number> -  Show the demon's pitch.
mesh span from <number> - Show the demon's net span origin.
mesh span to <number> - Show the demon's net span destination.
mesh span desc <number> - Show the demon's net span description
mesh span span <number> - Show the demon's net span.
mesh span <number> - Show the demon's net span details.
mesh number <name> - Get the number for that demon name."""

#FIXME: Capitalise names correctly for display

class MuDemonsPlugin(Plugin):

    def generate_response(self, arguments):
        result = "Unknown command: " + ' '.join(arguments)
        if len(arguments) == 1:
            if ln.is_a_demon_name(arguments[0]):
                number = ln.mesh_name_number(arguments[0])
                result = ln.PANDEMONIUM_MATRIX[number]
            elif ln.is_a_mesh_number(arguments[0]):
                result = ln.PANDEMONIUM_MATRIX[int(arguments[0])]
            else:
                # Probably not for us, so ignore
                result = False
        elif arguments[0] == 'number':
            result = arguments[1] + ' mesh number: ' \
                + str(ln.mesh_name_number(arguments[1]))
        elif arguments[0] == 'name':
            result = arguments[1].zfill(2) + ' demon name: ' \
                + ln.mesh_number_name(arguments[1])
        elif arguments[0] == 'names':
            result = arguments[1].zfill(2) + ' names: ' \
                + ', '.join(ln.mesh_number_names(arguments[1]))
        elif arguments[0] == 'clicks':
            result = arguments[1].zfill(2) + ' clicks: ' \
                + ln.mesh_number_clicks(arguments[1])
        elif arguments[0] == 'pitch':
            result = arguments[1].zfill(2) + ' pitch: ' \
                + ln.mesh_number_pitch(arguments[1])
        elif arguments[0] == 'span':
            if arguments[1] == 'from':
                result = arguments[2].zfill(2) + ' span from: ' \
                    + ln.mesh_number_net_span_from(arguments[2])
            elif  arguments[1] == 'to':
                result = arguments[2].zfill(2) + ' span to: ' \
                    + ln.mesh_number_net_span_to(arguments[2])
            elif arguments[1] == 'desc':
                result = arguments[2].zfill(2) + ' span description: ' \
                    + ln.mesh_number_net_span_desc(arguments[2])
            elif arguments[1] == 'span':
                result = arguments[2].zfill(2) + ' span: ' \
                    + ln.mesh_number_net_span_span(arguments[2])
            else:
                result = arguments[1].zfill(2) + ' span: ' \
                    + ln.mesh_number_net_span(arguments[1])
        return result

    def process_message(self, data):
        message = False
        response = False
        my_id = self.slack_client.server.login_data['self']['id']
        at_message_prefix = u'<@{0}>'.format(my_id)
        # Respond to DMs or addresses
        if data['channel'].startswith('D'):
            message = data['text']
        elif data['text'].startswith(at_message_prefix):
            message = data['text'][len(at_message_prefix):].strip()
        if message:
            if message.lower() == 'help':
                response = HELP
            elif message.strip().startswith('mesh'):
                arguments = [argument.lower()
                             for argument in message.split(' ')[1:]]
                response = self.generate_response(arguments)
        if response:
            self.outputs.append([data['channel'], response])
