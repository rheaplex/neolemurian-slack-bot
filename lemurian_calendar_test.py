from lemurian_calendar import *

D1D = timedelta(days=1)
D2Y = timedelta(days=MU_DOUBLE_YEAR_PERIOD)
D2C = timedelta(days=MU_TWO_CYCLES)
D2CI = timedelta(days=MU_TWO_CYCLES_PLUS_INTERCALATION)
DAEON = timedelta(days=MU_AEON)

MU_TEST_DATES_MU = [
    ('111:111111', MU_EPOCH),
    ('111:122112', date(2016, 6, 17)), # Known date
    ('111:122211', date(2016, 6, 25)), # Known date
    ('111:333333', MU_EPOCH + D2Y - D1D),
    ('112:111111', MU_EPOCH + D2Y),
    ('113:111111', MU_EPOCH + D2CI),
    ('113:333333', MU_EPOCH + (DAEON - ((D2CI * 12) + (D1D * 2)))),
    ('121:111111', MU_EPOCH + D2CI + D2Y),
    ('122:111111', MU_EPOCH + (D2CI * 2)),
    ('123:111111', MU_EPOCH + (D2CI * 2) + D2Y),
    ('131:111111', MU_EPOCH + (D2CI * 3)),
    ('332:111111', MU_EPOCH + DAEON - (D2CI + D1D)),
    ('332:333333', MU_EPOCH + DAEON - ((D2CI - D2Y) + (D1D * 2))),
    ('333:111111', MU_EPOCH + DAEON - (D2Y + D1D)),
    ('333:333333', MU_EPOCH + DAEON - (D1D * 2)),
]

MU_TEST_DATES_GREGORIAN = MU_TEST_DATES_MU + [
    ('111:111111', MU_EPOCH + DAEON - D1D), # 2070
    ('111:111111', MU_EPOCH + (DAEON * 2) + D1D), # 2124
    ('111:111111', MU_EPOCH + (DAEON * 3)), # 2178
    ('111:111111', MU_EPOCH + (DAEON * 4) + (D1D * 2)), # 2232
    ('111:111111', MU_EPOCH + (DAEON * 5) + D1D), # 2286
    ('111:111111', MU_EPOCH - (DAEON * 25) - (D1D * 15)), # 666
    ('111:111111', MU_EPOCH - (DAEON * 5) - (D1D * 5)), # 1746
    ('111:111111', MU_EPOCH - (DAEON * 4) - (D1D * 3)), # 1800
    ('111:111111', MU_EPOCH - (DAEON * 3) - (D1D * 4)), # 1854
    ('111:111111', MU_EPOCH - (DAEON * 2) - (D1D * 2)), # 1908
    ('111:111111', MU_EPOCH - DAEON - (D1D * 3)), # 1962
    ('121:331332', date(1970, 1, 1), 122), # UNIX Epoch
    ('333:' + MU_INTERCALATION_DAY_PREFIX + '3', MU_EPOCH - D1D),
    ('112:' + MU_INTERCALATION_DAY_PREFIX + '3', MU_EPOCH + D2CI - D1D),
]

if __name__ == '__main__':
    """Test some dates"""
    # We subtract one day from various additions because the epoch is the first day

    tests = 0
    passed = 0
    print("## Gregorian To Zygotriadic")
    for d in MU_TEST_DATES_GREGORIAN:
        mu = str(mu_gregorian_to_zygotriadic(d[1]))
        ok = mu == d[0]
        print('%s is %s, should be %s : %s' %  (d[1], mu, d[0],
                                                ['FAIL', 'PASS'][ok]))
        tests += 1
        passed += int(ok)
    print("## Zygotriadic To Gregorian")
    for d in MU_TEST_DATES_MU:
        mu = d[0].split(':')
        greg = mu_zygotriadic_to_gregorian(mu[1], mu[0])
        ok = greg == d[1]
        print('%s is %s, should be %s : %s' %  (d[0], greg, d[1],
                                                ['FAIL', 'PASS'][ok]))
        tests += 1
        passed += int(ok)
    print('Tests:      %i/%i passed.' % (passed, tests))
